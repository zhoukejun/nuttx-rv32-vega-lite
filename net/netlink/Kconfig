#
# For a description of the syntax of this configuration file,
# see the file kconfig-language.txt in the NuttX tools repository.
#

menu "Netlink Socket Support"

config NET_NETLINK
	bool "Netlink socket support"
	default n
	---help---
		Enable support for Netlink-like IPC sockets that will permit user-
		space applications to interact with network services.

		This logic is a WIP.  Currently only fragmentary support is
		available, not enough to actually do anything of consequence.
		Only the following features are implemented at this time:

		  NETLINK_ROUTE capability to read the ARP table.

if NET_NETLINK

config NETLINK_CONNS
	int "Number of Netlink connections"
	default 4
	---help---
		Maximum number of Netlink connections (all tasks).

config NETLINK_MAXPENDING
	int "Max pending responses"
	default 1
	---help---
		This defines the maximum number of threads that can be waiting for
		a NetLink response.  If there is never more than one recv() or
		recvfrom() per socket, then there need be only 1.  This only
		accounts for a perverse case where more than one thread is waiting
		on recv() or recvfrom().

config NETLINK_SIGNAL
	int "Response notification signal"
	default 15
	---help---
		This is the signal number that is used to wake up threads waiting
		for a response to be received.

menu "Netlink Protocols"

config NETLINK_ROUTE
	bool "Netlink Route protocol"
	default n
	---help---
		Support the NETLINK_ROUTE protocol option.

if NETLINK_ROUTE

config NETLINK_DISABLE_GETLINK
	bool "Disable RTM_GETLINK support"
	default n
	---help---
		RTM_GETLINK is used to enumerate network devices.

config NETLINK_DISABLE_GETNEIGH
	bool "Disable RTM_GETNEIGH support"
	default n
	---help---
		RTM_GETNEIGH is used to retrieve Neighbor/ARP tables.

config NETLINK_DISABLE_GETROUTE
	bool "Disable RTM_GETROUTE support"
	default n
	---help---
		RTM_GETROUTE is used to retrieve routing tables.

endif # NETLINK_ROUTE
endmenu # Netlink Protocols
endif # NET_NETLINK
endmenu # Netlink Socket Support

